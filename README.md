# Genome Dashboard

Genome Dashboard is a web application (node.js) that shows a gallery of genome shortcuts for [Integrated Genome Browser](https://bioviz.org).

The Dashboard shows an image for every species and genome available from IGB's main Quickload site. 

Clicking a species image in the Dashboard tells IGB to open the latest genome for that species. (See Figure 1.)

Also, you can open earlier genomes, if available. (See Figure 2.)

For more information about using IGB, visit:

* [Users' Guide](https://wiki.transvar.org/display/igbman/Home)
* [IGB YouTube](https://www.youtube.com/channel/UC0DA2d3YdbQ55ljkRKHRBkg)

Figure 1. Genome Dashboard with genome shortcut images.
![Figure 1. Genome Dashboard with shortcut images](images/genome-dashboard-1.png)

Figure 2. Other genome versions available.
![Figure 1. Other genome versions available.](images/genome-dashboard-2.png)

* * *

## Quick start - running Genome Dashboard on your local computer

**Step 1.** Install `npm` (node package manager) and `nodejs`. 

On Ubuntu, install with:

```
sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm
```

On CentOS, install with:

```
sudo yum update
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install nodejs
```

**Step 2.** Install require plugins

Clone the repository, change into the project folder, and run the following command to install plugins required for the Genome Dashboard to run.

```
npm install
```

**Step 3.** Start the Genome Dashboard application

Change into the project folder and run:


```
node ./bin/www
```

Alternatively, if you have `nodemon` installed, run the following command:

```
nodemon
```

**Step 4.** Download and start IGB

Download and run the installer for your platform from [Bioviz.org](https://bioviz.org). 

**Step 5.** Open Genome Dashboard

Open [localhost:3000](http://localhost:3000) in your Web browser and click and image to load the genome into IGB.

* * *

## Deploy on EC2 (CentOS)

#### Update system and install required software

```
sudo yum update
sudo yum git
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install nodejs
```

Install PM2 for Nodejs (Required to start Node application as service)

```
sudo npm install -g pm2
```

Clone the project, change into the cloned directory, and run the following command:

```
sudo pm2 start bin/www
```

This command starts the node service. Now we need to make it a linux demon so that it starts when the system boots. To do that run the following command:

```
sudo pm2 startup systemv
```

That is all we need on the Nodejs side.

#### Configure Apache

To access the Node.js script from the web, install the Apache modules proxy and proxy_http with the commands:

```
sudo yum install httpd24
sudo yum install mod24_ssl.x86_64
```

Restart apache service

```
sudo service httpd restart
```

Replace content in your default-site.conf file found at `/etc/httpd/conf.d/`.

Edit Apache configuration file:


```
sudo nano /etc/httpd/conf.d/default-site.conf
```

Add:

```
<VirtualHost *:80>
  ServerName <DomainName>
  RewriteEngine on
  RewriteCond %{SERVER_NAME} =<DomainName>
  RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
  ErrorLog logs/error.log
  CustomLog logs/access.log combined
</VirtualHost>
```

Where `<DomainName>` is a registered domain name.

To support https, replace content inside `ssl.conf`

```
sudo nano /etc/httpd/conf.d/ssl.conf
```

Add:

```
Listen 443 https

SSLCryptoDevice builtin

<VirtualHost _default_:443>

DocumentRoot "/var/www/html"
ServerName <DomainName>

ErrorLog logs/ssl_error_log
TransferLog logs/ssl_access_log
LogLevel warn

SSLEngine on
SSLCertificateFile /home/ec2-user/crt/<DomainName.crt>
SSLCertificateKeyFile /home/ec2-user/crt/<DomainName.key>
SSLCertificateChainFile /home/ec2-user/crt/<CertificateAuthority.crt>

<FilesMatch "\.(cgi|shtml|phtml|php)$">
    SSLOptions +StdEnvVars
</FilesMatch>
<Directory "/var/www/cgi-bin">
    SSLOptions +StdEnvVars
</Directory>

BrowserMatch "MSIE [2-5]" \
         nokeepalive ssl-unclean-shutdown \
         downgrade-1.0 force-response-1.0

CustomLog logs/ssl_request_log \
          "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

    ProxyRequests Off
    ProxyPreserveHost On
    ProxyVia Full
    <Proxy *>
        Require all granted
    </Proxy>

    <Location "/">
        ProxyPass http://<EC2IPAddress>:3000/
        ProxyPassReverse http://<EC2IPAddress>:3000/
    </Location>
</VirtualHost>

```

Replace:

* `<EC2IPAddress>` with the EC instance public IP address
* `<DomainName>` with your site's domain name (e.g., example.com)
* `<DomainName.crt>` with your site's SSL certificate
* `<DomainName.key>` with your site's private key 
* `<SSLCertificateChainFile>` with the signing authority's public key


Restart the apache server:

```
sudo service httpd restart
``` 

* * *

## Credits

* Code and design: [Sameer Shanbhag](mailto:sshanbh1@uncc.edu)
* Concept: Nowlan Freese and Ann Loraine