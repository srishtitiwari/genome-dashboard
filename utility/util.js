let fetch = require('node-fetch');

const CONTENT_TXT = 'http://igbquickload.org/quickload/contents.txt';
const SPECIES = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/synonym-lookup/src/main/resources/species.txt';
const SYNONYMS = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/synonym-lookup/src/main/resources/synonyms.txt';

const ALLQUICKLOADS = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/igb-preferences/src/main/resources/igbDefaultPrefs.json';


let mergeAllQuickLoadSites = async () => {
        try{
            const response = await fetch(ALLQUICKLOADS);
            const tempQuickData = await response.json();
            let quickLoadData = tempQuickData.prefs.server;
            let CompleteFactoryData = [];
            for (const factory of quickLoadData) {
                if(factory.factoryName === 'quickload') {
                    try {
                        let tempData = await fetch(factory.url + '/contents.txt');
                        let data = await tempData.text();
                        let finalData = data.split('\n');
                        CompleteFactoryData = [...CompleteFactoryData, ...finalData]
                    } catch (error) {
                        console.log(error.message)
                    }
                }
            }
            return CompleteFactoryData;
        } catch (error) {
            console.log(error.message)
        }
};


var getContent = async () => {
    let genomeData = {};
    let commonName = {};
    let scientificName = {};
    let synonyms = {};
    try {
        let content_arr = await mergeAllQuickLoadSites();
        content_arr.forEach(element => {
            if (element.trim().length > 0) {
                let temp_name = element.split('\t')[0].split('_');
                let final = temp_name[0] + '_' + temp_name[1];
                if (!final.includes(undefined)) {
                    temp = element.split('\t')[1].split(' ')[0] + ' ' + element.split('\t')[1].split(' ')[1];
                    if (genomeData[final] !== undefined) {
                        if(!scientificName[final].includes(temp)){
                            scientificName[final].push(temp); // = genomeData[final].
                        }
                        if(!genomeData[final].includes(element.split('\t')[0])){
                            genomeData[final].push(element.split('\t')[0]);
                        }
                    } else {
                        genomeData[final] = [element.split('\t')[0]];
                        scientificName[final] = [temp];
                    }
                }
            }
        });
    } catch (error) {
        console.log(error.message);
    }
    try {
        await fetch(CONTENT_TXT)
            .then((response) => response.text())
            .then(newResp => {
                let content_arr = newResp.split('\n');
                content_arr.forEach(element => {
                    if (element.trim().length > 0) {
                        let temp_name = element.split('\t')[0].split('_');
                        let final = temp_name[0] + '_' + temp_name[1];
                        if (!final.includes(undefined)) {
                            temp = element.split('\t')[1].split(' ')[0] + ' ' + element.split('\t')[1].split(' ')[1];
                            if (genomeData[final] !== undefined) {
                                if(!scientificName[final].includes(temp)){
                                    scientificName[final].push(temp); // = genomeData[final].
                                }
                                if(!genomeData[final].includes(element.split('\t')[0])){
                                    genomeData[final].push(element.split('\t')[0]);
                                }
                            } else {
                                genomeData[final] = [element.split('\t')[0]];
                                scientificName[final] = [temp];
                            }
                        }
                    }
                });
            });
    } catch (error) {
        console.log(error.message);
    }
    try {
        await fetch(SPECIES)
            .then((response) => response.text())
            .then(newResp => {
                let content_arr = newResp.split('\n');
                content_arr.forEach(element => {
                    if (element.trim().length > 0) {
                        let temp_name = element.split('\t');
                        let bioname = temp_name.filter(data => data.includes('_'));
                        let final = bioname[0].replace(/\r/g, '');
                        final = final.split('_');
                        final = final[0] + '_' + final[1];
                        if (!final.includes(undefined)) {
                            if (commonName[final] !== undefined) {
                                commonName[final].push(element.split('\t')[1][0].toUpperCase() + element.split('\t')[1].substr(1)) // = genomeData[final].
                            } else {
                                commonName[final] = [element.split('\t')[1][0].toUpperCase() + element.split('\t')[1].substr(1)]
                            }
                        }
                    }
                });
            });
    } catch (error) {
        console.log(error.message);
    }
    try {
        await fetch(SYNONYMS)
            .then((response) => response.text())
            .then(newResp => {
                let content_arr = newResp.split('\n');
                content_arr.forEach(element => {
                    let temp_name = element.split('\t')[0];
                    if (!temp_name.includes(undefined)) {
                        if (synonyms[temp_name] !== undefined) {
                            synonyms[temp_name].push(element.split('\t').slice(1));
                        } else {
                            synonyms[temp_name] = element.split('\t').slice(1);
                        }
                    }
                });
            });
    } catch (error) {
        console.log(error.message);
    }
    sorted_genomeData = Object.keys(genomeData)
        .sort().reduce(function(Obj, key) {
            Obj[key] = genomeData[key];
            return Obj;
        }, {});
    return {'genomeData': sorted_genomeData, 'commonName': commonName, 'scientificName': scientificName, 'Synonyms':synonyms};
};

module.exports.getContent = getContent;




